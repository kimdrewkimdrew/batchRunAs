#-*- coding:utf-8 -*-

import os
import sys
import time
import ctypes
import win32gui
import webbrowser
import win32console

# 백그라운드 실행
win32gui.ShowWindow(win32console.GetConsoleWindow(), 0)

def main():

    # Temp 폴더에 파일생성
    fLoc1 = os.environ["TEMP"] + "\\Temp_Tmp.bat"
    fLoc2 = os.environ["TEMP"] + "\\Temp_Tmp.vbs"

    # Temp_Tmp.bat 
    # 관리자 권한으로 실행시킬 배치파일 내용 작성
    bFile = """
    
    """
    
    # Temp_Tmp.vbs
    vFile = """
    Set ws = CreateObject("Wscript.Shell")
    ws.run "%s", 0, false
    """ %fLoc1

    # 파일 생성
    f1 = open(fLoc1, "w"); f1.write(bFile); f1.close()
    f2 = open(fLoc2, "w"); f2.write(vFile); f2.close()

    # CMD 말고 웹 브라우저로 파일 실행
    webbrowser.open("file:///%s" %fLoc2)

    # 2초 기다리기
    time.sleep(2)

    # 파일 삭제
    os.remove(fLoc1); os.remove(fLoc2)

# 관리자 권한 체크
def isAdmin():
    
    try: return ctypes.windll.shell32.IsUserAnAdmin()
    except: return False

# 관리자면 main() 함수 실행, 아니면 관리자로 재실행
if isAdmin(): main()
else: ctypes.windll.shell32.ShellExecuteW(None, u"runas", unicode(sys.executable), unicode(__file__), None, 1)
